```plantuml
left to right direction

Title CANTEEN AUTOMATION SYSTEM

Main-->(Home)
Main-->(AboutUs)
Main-->(Contact Us)
Main-->(Follow Us On)
Main-->(Log In)
(Log In)-->(Register)
(Register)-->(Admin)
(Register)-->(Staff)
(Register)-->(User)
Main<--(Logout)


Admin-[bold,#blue]->(Logout)
Admin-[bold,#blue]->(Add Details)
(Add Details)-[bold,#blue]->(Add Inventory details)
(Add Details)-[bold,#blue]->(Add Food)
(Add Details)-[bold,#blue]->(Add Offer)
Admin-[bold,#blue]->(Remove Food)
Admin-[bold,#blue]->(Update)
Admin-[bold,#blue]->(Order Forecasting)
Admin-[bold,#blue]->(Report)
(Report)-[bold,#blue]->(Sales As Per Date)
(Report)-[bold,#blue]->(Sales As Per Time)
(Report)-[bold,#blue]->(Total Earning)


Staff-[bold,#green]->(Logout)
Staff-[bold,#green]->(Order Forecasting)
Staff-[bold,#green]->(Offer Details)
Staff-[bold,#green]->(Customer Order Details)

User-[bold,#black]->(Logout)
User-[bold,#black]->(Select Food Type)
(Select Food Type)-[bold,#black]->(Starter)
(Select Food Type)-[bold,#black]->(Main Course)
(Select Food Type)-[bold,#black]->(Dessert)
(Starter)-[bold,#black]->(Item 1)
(Starter)-[bold,#black]->(Item 2)
(Starter)-[bold,#black]->(Item 3)
(Main Course)-[bold,#black]->(Item M1)
(Main Course)-[bold,#black]->(Item M2)
(Main Course)-[bold,#black]->(Item M3)
(Dessert)-[bold,#black]->(Item D1)
(Dessert)-[bold,#black]->(Item D2)
(Dessert)-[bold,#black]->(Item D3)
User-[bold,#black]->(List Of Added Item)
User-[bold,#black]->(Bill Receipt)
User-[bold,#black]->(Feedback And Rating)

```